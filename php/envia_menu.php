<?php
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

$objDatos = json_decode(file_get_contents("php://input"));

$suscriptores = $objDatos->data->suscriptores;
$platillos = $objDatos->data->platillos;
$pastas = $objDatos->data->pastas;
$aguas = $objDatos->data->aguas;
$sopas = $objDatos->data->sopas;

//print_r($objDatos);

$titulo    = 'La Parrilla Express Menú del Día';

$msg = "<body>";
$msg = "<h1>La Parrilla Express de Ciudad Guzmán</h1>";
$msg .= "<img src='http://www.aidihosting.com/proyectos/laparrillaexpress/imagenes/la-parrilla-express.jpg' width='600' height='500' alt='La Parrilla Express'>";
$msg .= "<p><b>Estimado cliente</b>,<br> Para nosotros es un placer hacerle llegar nuestro menu del día de hoy: <strong>". date('Y-m-d') ."</strong>.</p></p>Esperamos sea de su agrado y contar hoy con su agradable visita.<br><b> ¡Le deseamos un buen día!</b>.</p>";

$msg .= "<ul>";

	$msg .= "<li><h2>Platillos</h2>";
foreach ($platillos as $platillo) {
	if($platillo->activo){
		$msg .= "<ul>";
		$msg .= "<li><h3><strong>" . $platillo->title . "</strong></h3>";
		$msg .= "<ul>";
		$msg .= "<li><strong>Descripción: </strong>" . $platillo->description . "</li>";
		$msg .= "<li><strong>Tipo de carne: </strong>" . $platillo->tipo . "</li>";
		$msg .= "</ul>";
		$msg .= "</li>";
		$msg .= "</ul>";
	}
}
	$msg .= "</li>";

	$msg .= "<li><h2>Pastas</h2>";
foreach ($pastas as $pasta) {
	if($pasta->activo){
		$msg .= "<ul>";
		$msg .= "<li><h3><strong>" . $pasta->title . "</strong></h3>";
		$msg .= "<ul>";
		$msg .= "<li><strong>Descripción: </strong>" . $pasta->description . "</li>";
		$msg .= "</ul>";
		$msg .= "</li>";
		$msg .= "</ul>";
	}
}
	$msg .= "</li>";

	$msg .= "<li><h2>Sopas</h2>";
foreach ($sopas as $sopa) {
	if($sopa->activo){
		$msg .= "<ul>";
		$msg .= "<li><h3><strong>" . $sopa->title . "</strong></h3>";
		$msg .= "<ul>";
		$msg .= "<li><strong>Descripción: </strong>" . $sopa->description . "</li>";
		$msg .= "</ul>";
		$msg .= "</li>";
		$msg .= "</ul>";
	}
}
	$msg .= "</li>";

	$msg .= "<li><h2>Aguas frescas</h2>";
foreach ($aguas as $agua) {
	if($agua->activo){
		$msg .= "<ul>";
		$msg .= "<li><h3><strong>" . $agua->title . "</strong></h3>";
		$msg .= "<ul>";
		$msg .= "<li><strong>Descripción: </strong>" . $agua->description . "</li>";
		$msg .= "</ul>";
		$msg .= "</li>";
		$msg .= "</ul>";
	}
}
	$msg .= "</li>";

$msg .= "</ul>";
$msg .= "<p>Visite nuestro sitio web <a href='http://laparrillaexpress.com'>laparrillaexpress.com</a> donde encontrará nuestro menú del día.";
$msg .= "</body>";

//require_once('phpmailer/class.phpmailer.php');
function sendMail($emailto,$subject,$body,$altbody){

$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
$headers .= 'From: <menudeldia@laparrillaexpress.com>' . "\r\n";

if(!mail($emailto,$subject,$body,$headers)) {
	    return 'EL/Los mensaje(s) no ha(n) sido enviado(s). Error: ' . $mail->ErrorInfo;
	} else {
	    return 'El/Los mensaje(s) ha(n) sido enviado(s)';
	}
}

foreach ($suscriptores as $suscriptor) {
	if($suscriptor->activo){
		$response = sendMail($suscriptor->email,$titulo,$msg,strip_tags($msg));
	}
}

echo $response;

?>